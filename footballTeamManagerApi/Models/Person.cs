﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace footballTeamManagerApi.Models
{
    public class Person
    {
        public int Id { get; set; }
        public int Address_Id { get; set; } //Optional && FK
        public string First_Name { get; set; } 
        public string Last_Name { get; set; } 
        public DateTime Date_Of_Birth { get; set; } 

    }
}
