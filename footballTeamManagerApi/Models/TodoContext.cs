﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using footballTeamManagerApi.Models;


namespace footballTeamManagerApi.Models
{
    public class TodoContext : DbContext
    {
        public TodoContext(DbContextOptions<TodoContext> options)
           : base(options)
        {
        }
        public DbSet<footballTeamManagerApi.Models.TodoItem> TodoItem { get; set; }

        public DbSet<footballTeamManagerApi.Models.Team> Team { get; set; }

    }
}
