﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace footballTeamManagerApi.Models
{
    public class Match_Position
    {
        public int Player_Id { get; set; } //FK
        public int Match_Id { get; set; } //FK
        public string Position { get; set; } //Optional
    }
}
