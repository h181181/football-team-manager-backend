﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace footballTeamManagerApi.Models
{
    public class Team
    {
        public int Team_Id { get; set; }

        public int Association_Id { get; set; }

        public int Coach_Id { get; set; }

        public int Owner_Id { get; set; }

        public int Location_Id { get; set; }
    }
}
