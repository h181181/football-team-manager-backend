﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace footballTeamManagerApi.Models
{
    public class Goal_Type
    {
        public int Id { get; set; }
        public string Type { get; set; }
    }
}
