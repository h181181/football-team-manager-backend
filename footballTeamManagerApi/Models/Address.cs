﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace footballTeamManagerApi.Models
{
    public class Address
    {
        public int Id { get; set; }
        public string Address_Line_1 { get; set; }
        public string Address_Line_2 { get; set; } //Optional
        public string Address_Line_3 { get; set; } //Optional
        public int Postal_Code { get; set; }
        public string City { get; set; }
        public string Country { get; set; }

    }   
}
