﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace footballTeamManagerApi.Models
{
    public class Player
    {
        public int Id { get; set; }
        public int Person_Id { get; set; } //FK
        public int Team_Id { get; set; } //FK
        public string Normal_Position { get; set; } //Optional
        public int Number { get; set; } //Optional
    }
}
