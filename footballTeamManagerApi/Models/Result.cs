﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace footballTeamManagerApi.Models
{
    public class Result
    {
        public int Match_Id { get; set; } //FK
        public int Team_Id { get; set; } //FK
        public int Score { get; set; }
        public WinOrLoss Outcome { get; set; }


        public enum WinOrLoss
        {
            VICTORY,
            LOSS
        }
    }

}
