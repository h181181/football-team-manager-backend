﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace footballTeamManagerApi.Models
{
    public abstract class User
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Username {get; set;}
        public string Password { get; set; }
        public UserType UserTypes { get; set; }
        public enum UserType { Admin, NormalUser, Anonymous}

        public User(string name, string username, string password, UserType types){
            Name = name;
            Username = username;
            Password = password;
            UserTypes = types;
        }
            
    }
}
