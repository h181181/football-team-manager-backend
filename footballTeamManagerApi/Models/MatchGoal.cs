﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace footballTeamManagerApi.Models
{
    public class Match_Goal
    {
        public int Id { get; set; }
        public int Player_Id { get; set; } //FK
        public int Goal_Type_Id { get; set; } //FK
        public int Match_Id { get; set; } //FK
        public string Description { get; set; } //Optional
    }
}
