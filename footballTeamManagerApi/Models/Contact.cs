﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace footballTeamManagerApi.Models
{
    public class Contact
    {
        public int Id { get; set; }
        public int Person_Id { get; set; } //FK
        public string Contact_Type { get; set; }
        public string Contact_Detail { get; set; }

    }
}
