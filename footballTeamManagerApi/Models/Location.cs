﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace footballTeamManagerApi.Models
{
    public class Location
    {
        public int Id { get; set; }
        public int Address_ID { get; set; }
        public string Name { get; set; }
        public string Description {get; set;}
    }
}
