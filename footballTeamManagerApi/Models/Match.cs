﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace footballTeamManagerApi.Models
{
    public class Match
    {
        public int Id { get; set; }
        public DateTime Match_Date { get; set; }
        public int Home_Team_Id { get; set; } //FK
        public int Away_Team_Id { get; set; } //FK
        public int Season_Id { get; set; } //FK
        public int Location_Id { get; set; } //FK
    }
}
